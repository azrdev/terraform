## Presentation

(Necessarily link to an issue. If it doesn't exist, please create one.)

Fixes #999

## Checklist

* Documented:
    * [ ] `README.md` reflects any job, variable or whichever visible change
    * [ ] `kicker.json` reflects any job, variable or whichever visible change
* Tested & examplified:
    * [ ] (url to a project sample successfully proving the merge request fixes the issue)

/label ~"kind/fix"
